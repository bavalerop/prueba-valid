package com.microservice.valid.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.valid.user.dto.ErrorInfo;
import com.microservice.valid.user.entity.UserEntity;
import com.microservice.valid.user.exceptions.ExceptionErrorUser;
import com.microservice.valid.user.service.IUserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/user")
@Tag(name = "UserEntity Controller", description = "Controlador para los usuarios")
public class UserController {

	@Autowired
	private IUserService useService;
	
	//GET
	@Operation(summary = "Get Usuarios", description = "Muestra la lista de usuarios")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "Respuesta Exitosa", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserEntity.class)))),
		@ApiResponse(responseCode = "201", description = "Creado exitosamente", content = @Content),
		@ApiResponse(responseCode = "204", description = "No se encontraron resultados", content = @Content),
		@ApiResponse(responseCode = "401", description = "No tiene autorización para ver el recurso", content = @Content),
		@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content),
		@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content),
		@ApiResponse(responseCode = "500", description = "Error de servidor", content = @Content)
	})
	@GetMapping(value = "/", produces = "application/json")
	public ResponseEntity<?> list() throws Exception {
		List<UserEntity> listUser = useService.findAllUsers();
		
		return new ResponseEntity<>(listUser, HttpStatus.OK);

	}
	
	//GET BY NAME
	@Operation(summary = "Get User By Name", description = "Muestra el Usuario a buscar por nombre")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "Respuesta Exitosa", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserEntity.class)))),
		@ApiResponse(responseCode = "201", description = "Creado exitosamente", content = @Content),
		@ApiResponse(responseCode = "204", description = "No se encontraron resultados", content = @Content),
		@ApiResponse(responseCode = "401", description = "No tiene autorización para ver el recurso", content = @Content),
		@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content),
		@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content),
		@ApiResponse(responseCode = "500", description = "Error de servidor", content = @Content)
	})
	@GetMapping(value = "/getByName/{name}", produces = "application/json")
	public ResponseEntity<?> findByName(
			@Parameter(description="Nombre del usuario a buscar.")
			@PathVariable String name
			)  throws Exception {
		List<UserEntity> listUsers = useService.findUsersByName(name);
		if (listUsers != null) {
			return new ResponseEntity<>(listUsers, HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	//GET BY ID
	@Operation(summary = "Get User By ID", description = "Muestra el usuario a buscar por ID")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "Respuesta Exitosa", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserEntity.class)))),
		@ApiResponse(responseCode = "201", description = "Creado exitosamente", content = @Content),
		@ApiResponse(responseCode = "204", description = "No se encontraron resultados", content = @Content),
		@ApiResponse(responseCode = "401", description = "No tiene autorización para ver el recurso", content = @Content),
		@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content),
		@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content),
		@ApiResponse(responseCode = "500", description = "Error de servidor", content = @Content)
	})
	@GetMapping(value = "/getById/{id}", produces = "application/json")
	public ResponseEntity<?> findByID(
			@Parameter(description="ID del usuario a buscar.")
			@PathVariable Long id
			)  throws Exception {
		UserEntity user = useService.findUserById(id);
		if (user != null) {
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}
	
	//POST
	@Operation(summary = "Post User", description = "Guarda el usuario ingresado")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "Respuesta Exitosa", content = @Content),
		@ApiResponse(responseCode = "201", description = "Creado exitosamente", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserEntity.class)))),
		@ApiResponse(responseCode = "204", description = "No se encontraron resultados", content = @Content),
		@ApiResponse(responseCode = "401", description = "No tiene autorización para ver el recurso", content = @Content),
		@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content),
		@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content),
		@ApiResponse(responseCode = "500", description = "Error de servidor", content = @Content)
	})
	@PostMapping(value = "/save", produces = "application/json")
	public ResponseEntity<?> create(
			@Parameter(description="Usuario a guardar.", required=true) @Valid @RequestBody UserEntity user) throws Exception {
		try {
			return new ResponseEntity<>(useService.save(user), HttpStatus.CREATED);
		}catch (ExceptionErrorUser e2) {
			ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), e2.getMessage(), "/api/user/save");
			return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(Exception e) {
			ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error interno del servidor", "/api/user/save");
			return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//PUT
	@Operation(summary = "Put User", description = "Actualiza el usuario ingresado")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "Respuesta Exitosa", content = @Content),
		@ApiResponse(responseCode = "201", description = "Actualizado exitosamente", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserEntity.class)))),
		@ApiResponse(responseCode = "204", description = "No se encontraron resultados", content = @Content),
		@ApiResponse(responseCode = "401", description = "No tiene autorización para ver el recurso", content = @Content),
		@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content),
		@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content),
		@ApiResponse(responseCode = "500", description = "Error de servidor", content = @Content)
	})
	@PutMapping(value = "/update", produces = "application/json")
	public ResponseEntity<?> update(
			@Parameter(description="Usuario a Actualizar.", required=true) @Valid @RequestBody UserEntity user) throws Exception {
						
			try {
				return new ResponseEntity<>(useService.save(user), HttpStatus.OK);
			}catch (ExceptionErrorUser e2) {
				ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), e2.getMessage(), "/api/user/update");
				return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
			} catch(Exception e) {
				ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error interno del servidor", "/api/user/update");
				return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
			}

	}
	
	//PUT BATCH
	@Operation(summary = "Put User in batch", description = "Actualiza los usuarios enviados")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "Respuesta Exitosa", content = @Content),
		@ApiResponse(responseCode = "201", description = "Actualizado exitosamente", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserEntity.class)))),
		@ApiResponse(responseCode = "204", description = "No se encontraron resultados", content = @Content),
		@ApiResponse(responseCode = "401", description = "No tiene autorización para ver el recurso", content = @Content),
		@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content),
		@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content),
		@ApiResponse(responseCode = "500", description = "Error de servidor", content = @Content)
	})
	@PutMapping(value = "/updateAll", produces = "application/json")
	public ResponseEntity<?> updateAll(
			@Parameter(description="Usuarios a Actualizar.", required=true) @Valid @RequestBody List<UserEntity> users) throws Exception {
						
			try {
				return new ResponseEntity<>(useService.saveAll(users), HttpStatus.OK);
			}catch (ExceptionErrorUser e2) {
				ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), e2.getMessage(), "/api/user/updateAll");
				return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
			} catch(Exception e) {
				ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error interno del servidor", "/api/user/updateAll");
				return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
			}

	}
	
	//DELETE
	@Operation(summary = "Delete user By ID", description = "Elimina el usuario ingresado por ID")
	@ApiResponses({
		@ApiResponse(responseCode = "200", description = "Respuesta Exitosa", content = @Content),
		@ApiResponse(responseCode = "201", description = "Eliminado exitosamente", content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserEntity.class)))),
		@ApiResponse(responseCode = "204", description = "No se encontraron resultados", content = @Content),
		@ApiResponse(responseCode = "401", description = "No tiene autorización para ver el recurso", content = @Content),
		@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content),
		@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content),
		@ApiResponse(responseCode = "500", description = "Error de servidor", content = @Content)
	})
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public ResponseEntity<?> delete(
			@Parameter(description="ID del usuario a eliminar.")
			@PathVariable Long id
			)throws Exception {
		
		try {
			useService.deleteById(id);
			ErrorInfo mes = new ErrorInfo(HttpStatus.OK.value(), "Eliminado Exitosamente", "/api/user/delete");
			return new ResponseEntity<ErrorInfo>(mes, HttpStatus.OK);
		}catch (ExceptionErrorUser e2) {
			ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), e2.getMessage(), "/api/user/delete");
			return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(Exception e) {
			ErrorInfo ex = new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error interno del servidor", "/api/user/delete");
			return new ResponseEntity<ErrorInfo>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
