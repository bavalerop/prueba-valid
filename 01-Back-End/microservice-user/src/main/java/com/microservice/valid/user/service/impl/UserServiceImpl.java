package com.microservice.valid.user.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservice.valid.user.entity.UserEntity;
import com.microservice.valid.user.exceptions.ExceptionErrorUser;
import com.microservice.valid.user.repository.IUserRepo;
import com.microservice.valid.user.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {
	
	@Autowired
	private IUserRepo repository;

	@Override
	public UserEntity save(UserEntity user) throws ExceptionErrorUser   {
		try {
			return repository.save(user);
		} catch (Exception e) {			
			e.printStackTrace(); 
			throw new ExceptionErrorUser("Error en los campos de ingreso");
		}
	}
	
	@Override
	public List<UserEntity> saveAll(List<UserEntity> users) throws ExceptionErrorUser   {
		try {
			return repository.saveAll(users);
		} catch (Exception e) {			
			e.printStackTrace(); 
			throw new ExceptionErrorUser("Error en los campos de ingreso");
		}
	}

	@Override
	public void deleteById(Long id) throws ExceptionErrorUser {
		try {
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace(); 
			throw new ExceptionErrorUser("Error, no existe el usuario a eliminar");
		}
		
	}

	@Override
	public UserEntity findUserById(Long id) {
		try {
			return repository.findById(id).get();
		} catch (Exception e) {
			e.printStackTrace(); 
			throw e;
		}
	}

	@Override
	public List<UserEntity> findAllUsers() {
		try {
			return repository.findAll();
		} catch (Exception e) {
			e.printStackTrace(); 
			throw e;
		}
	}

	@Override
	public List<UserEntity> findUsersByName(String name) {
		try {
			return repository.findUsersByName(name);
		} catch (Exception e) {
			e.printStackTrace(); 
			throw e;
		}
	}

}
