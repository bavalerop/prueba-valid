package com.microservice.valid.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.microservice.valid.user.entity.UserEntity;

public interface IUserRepo extends JpaRepository<UserEntity, Long>{
	
	@Query("SELECT a FROM UserEntity a WHERE UPPER(a.name) LIKE CONCAT('%',UPPER(:name),'%')")
	public List<UserEntity> findUsersByName(@Param("name") String name);
}
