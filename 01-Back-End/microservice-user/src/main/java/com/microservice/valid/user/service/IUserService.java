package com.microservice.valid.user.service;

import java.util.List;


import com.microservice.valid.user.entity.UserEntity;
import com.microservice.valid.user.exceptions.ExceptionErrorUser;

public interface IUserService {

	public UserEntity save(UserEntity user) throws ExceptionErrorUser;
	
	public List<UserEntity> saveAll(List<UserEntity> users) throws ExceptionErrorUser;

	public void deleteById(Long id) throws ExceptionErrorUser;

	public UserEntity findUserById(Long id);

	public List<UserEntity> findAllUsers();
	
	public List<UserEntity> findUsersByName(String name);
}
