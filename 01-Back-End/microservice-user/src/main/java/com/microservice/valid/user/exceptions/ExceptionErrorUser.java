package com.microservice.valid.user.exceptions;

public class ExceptionErrorUser extends Exception {


	private static final long serialVersionUID = 292805201630519553L;
	
	public ExceptionErrorUser(String message) {
		super(message);
	}

}
