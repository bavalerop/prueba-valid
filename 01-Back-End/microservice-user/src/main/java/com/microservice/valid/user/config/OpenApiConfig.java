package com.microservice.valid.user.config;

import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class OpenApiConfig {
	

	
	@Bean
    public OpenAPI customOpenAPI() {
		//Objetos de Informacion
		//Obj Contacto
		Contact con = new Contact();
		con.setName("Brayan Valero");
		con.setUrl("https://www.linkedin.com/in/brayan-andr%C3%A9s-valero-pinz%C3%B3n-512900159/");
		con.setEmail("bavalerop@gmail.com");
		//Obj Lincence
		License li = new License();
		li.setName(" Apache 2.0");
		li.setUrl("https://www.apache.org/licenses/LICENSE-2.0.html");
		List<Server> serv = new ArrayList<Server>();
		Server s1 = new Server();
		s1.setUrl("http://localhost:8082/");
		s1.setDescription("local");
		serv.add(s1);
        return new OpenAPI()
        		.servers(serv)
        		.components(new Components().addSecuritySchemes("basicScheme",
                        new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("basic")))
                .info(new Info()
                		.title("Spring Boot REST API Users")
                		.description("API para el manejo de Usuarios VALID")
                		.version("1.0.1")
                		.contact(con)
                		.license(li)
                );
    }
}
