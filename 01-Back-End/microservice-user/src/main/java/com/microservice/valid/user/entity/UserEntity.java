package com.microservice.valid.user.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@Table(name = "user")
@Schema(name = "User", description = "Objeto para almacenar los atributos de un Usuario")
public class UserEntity implements Serializable {

	private static final long serialVersionUID = 6507352907595406221L;
	
	@Schema(description = "Identificador unico del usuario", required = true, example = "1", hidden = true)
	@Id
	@NotEmpty
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private Long id;
	
	@NotNull
	@Pattern(regexp = "[a-zA-Z]")
	@Schema(description = "Nombres del usuario", required = true, example = "Brayan Andres")
	@Column(columnDefinition = "VARCHAR(100)", nullable = false, name = "user_name")
	private String name;
	
	@NotNull
	@Pattern(regexp = "[a-zA-Z]")
	@Schema(description = "Apellidos del usuario", required = true, example = "Valero Pinzon")
	@Column(columnDefinition = "VARCHAR(100)", nullable = false, name = "user_lastname")
	private String lastName;
	
	@NotNull
	@Schema(description = "Usuario Procesado", required = true, example = "false")
	@Column(columnDefinition = "BOOLEAN", nullable = false, name = "user_processed")
	private Boolean processed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", name=" + name + ", lastName=" + lastName + ", processed=" + processed + "]";
	}
	

}
