export class UserModel {
    id: number;
    name: String;
    lastName: String;
    processed: boolean; 

  constructor(id: number, name: String, lastName: String,  processed: boolean) {
    this.id = id;
    this.name = name;
    this.lastName = lastName;
    this.processed = processed;
  }
}