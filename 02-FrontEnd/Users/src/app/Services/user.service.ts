import { UserModel } from '../Models/User.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiService } from './api.service';




@Injectable({
  providedIn: 'root'
})
export class UserService {

  public rut: string;

  constructor(private api: ApiService) { }

  getUser(): Observable<UserModel[]> {
    return this.api.getService('/user/', '').pipe(
      map( response => response as UserModel[])
    );
  }

  getUserByName(name: String): Observable<UserModel[]> {
    return this.api.getService('/user/getByName/'+name, '').pipe(
      map( response => response as UserModel[])
    );
  }

  postUser(user: UserModel): Observable<any> {
    return this.api.postService('/user/save', '', user).pipe(
      map( response => response as object)
    );
   }

  putUser(user: any): Observable<any> {
    return this.api.putService('/user/update', '', user).pipe(
      map( response => response as object)
    );
  }

  putUserAll(users: any): Observable<any> {
    return this.api.putServiceAll('/user/updateAll', '', users).pipe(
     map( response => response as object)
   );
  }

  deleteUser(user: any): Observable<any> {
    return this.api.deleteService('/user/delete/' + user.id, '').pipe(
     map( response => response as object)
   );
  }
}
