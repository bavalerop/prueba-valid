import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bavs-title-page',
  templateUrl: './bavs-title-page.component.html',
  styleUrls: ['./bavs-title-page.component.scss']
})
export class BavsTitlePageComponent implements OnInit {

  @Input() title: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
