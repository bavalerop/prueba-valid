import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'bavs-toolbar-page',
  templateUrl: './bavs-toolbar-page.component.html',
  styleUrls: ['./bavs-toolbar-page.component.scss']
})
export class BavsToolbarPageComponent implements OnInit {

  @Input() idToolbar: any;
  // Variables btn ADD
  @Input() idAdd: string;
  @Input() titleAdd: string;
  @Input() dataPopoverAdd: string;
  @Input() buttonDisabledAdd: string;
  @Output() clickCustomAdd = new EventEmitter();
  // Variables btn REPORT
  @Input() idReport: string;
  @Input() titleReport: string;
  @Input() dataPopoverReport: string;
  @Input() buttonDisabledReport: string;
  @Output() clickCustomReport = new EventEmitter();
  // Variables btn EXCEL
  @Input() idExcel: string;
  @Input() titleExcel: string;
  @Input() dataPopoverExcel: string;
  @Input() buttonDisabledExcel: string;
  @Output() clickCustomExcel = new EventEmitter();
  // Variables btn CLEAR
  @Input() idClear: string;
  @Input() titleClear: string;
  @Input() dataPopoverClear: string;
  @Input() buttonDisabledClear: string;
  @Output() clickCustomClear = new EventEmitter();
  // Variables txt INPUT
  @Input() idInput: string;
  @Input() idControl: string;
  @Input() titleInput: string;
  @Input() PlaceHolder: string;
  @Input() inputDisabled: string;
  @Input() inputModel: string;
  @Output() inputModelChange = new EventEmitter<string>();
  // Variables btn CHANGE PROCESSED
  @Input() idChangePro: string;
  @Input() titleChangePro: string;
  @Input() dataPopoverChangePro: string;
  @Input() buttonDisabledChangePro: string;
  @Output() clickCustomChangePro = new EventEmitter();
  // Variables btn CHANGE INPROCESSED
  @Input() idChangeDesPro: string;
  @Input() titleChangeDesPro: string;
  @Input() dataPopoverChangeDesPro: string;
  @Input() buttonDisabledChangeDesPro: string;
  @Output() clickCustomChangeDesPro = new EventEmitter();  

  constructor() { 
    this.idAdd = "";
    this.buttonDisabledAdd = "false";

    this.idReport = "";
    this.buttonDisabledReport = "false";

    this.idExcel = "";
    this.buttonDisabledExcel = "false";

    this.idClear = "";
    this.buttonDisabledClear = "false";

    this.idControl = "";
    this.inputDisabled = "false";

    this.idChangePro = "";
    this.buttonDisabledChangePro = "false";

    this.idChangeDesPro = "";
    this.buttonDisabledChangeDesPro = "false";
  }

  ngOnInit(): void {
  }

  clickAdd() {
    this.clickCustomAdd.emit();
  }

  clickReport() {
    this.clickCustomReport.emit();
  }

  clickExcel() {
    this.clickCustomExcel.emit();
  }

  clickClear() {
    this.clickCustomClear.emit();
  }

  clickChangePro() {
    this.clickCustomChangePro.emit();
  }

  clickChangeDesPro() {
    this.clickCustomChangeDesPro.emit();
  }
}
