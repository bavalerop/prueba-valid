import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { AgGridModule } from 'ag-grid-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BavsTitlePageComponent } from './CustomComponent/bavs-title-page/bavs-title-page.component';
import { BavsToolbarPageComponent } from './CustomComponent/bavs-toolbar-page/bavs-toolbar-page.component';
import { BavsSpinnerPageComponent } from './CustomComponent/bavs-spinner-page/bavs-spinner-page.component';

@NgModule({
  declarations: [
    BavsTitlePageComponent,
    BavsToolbarPageComponent,
    BavsSpinnerPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 7000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    }),
    FormsModule,
    AgGridModule.withComponents([]),
    NgbModule
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    BavsTitlePageComponent,
    BavsToolbarPageComponent,
    BavsSpinnerPageComponent,
    AgGridModule,
    NgbModule
  ],
  providers: [
  ]
})
export class SharedModule { }
