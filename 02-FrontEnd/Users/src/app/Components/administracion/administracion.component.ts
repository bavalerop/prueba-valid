import { Component, OnInit, ViewChild }  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UserService } from '../../Services/user.service';
import { UserModel } from './../../Models/User.model';
declare var $: any;



@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['./administracion.component.scss']
})
export class AdministracionComponent implements OnInit {
  public load: boolean;
  public formUser: FormGroup;
  public formUserUpdate: FormGroup;

  //DataGlobal
  public filterModel: string;

  // Grid
  public columnDefs: any;
  public defaultColDef: any;
  public rowData: UserModel[];
  public gridApi;
  public rowDataUpdateBatch: any;

  // Modal
  public dataModal: UserModel;
  @ViewChild('ModalUpdate') ModalUpdate: any;
  @ViewChild('ModalCreate') ModalCreate: any;
  public closeResult = '';
  public modalReference: any;
  public indexUpdate: number;

  constructor(private toastr: ToastrService, private _userService: UserService, private modalService: NgbModal) { 
    this.filterModel = "";
    this.rowDataUpdateBatch = [];
    this.columnDefs = [
      { field: '', width: 20 },
      { headerName: 'Nombre', field: 'name', width: 220, resizable: true },
      { headerName: 'Apellido', field: 'lastName', width: 220, resizable: true },
      { headerName: 'Procesado', field: 'processed', width: 180, resizable: true},
      {
        headerName: 'OPT',
        field: '',
        cellRenderer: (data) => {
          // tslint:disable-next-line: max-line-length
          return `<button class="btn btn-danger btn-sm rounded-0 delete" type="button" data-toggle="DELETE" data-placement="top" title="Delete"><i class="fas fa-trash"></i></button>
                  <button class="btn btn-info btn-sm rounded-0 edit" type="button" data-toggle="UPDATE" data-placement="top" title="UPDATE">
                  <i class="fas fa-edit"></i></button>`;
        } , width: 220
      }
    ];
    this.defaultColDef = {
      headerCheckboxSelection: this.isFirstColumn,
      checkboxSelection: this.isFirstColumn,
    }
  }

  ngOnInit(): void {
    this.cargarUsers();  
    this.cargarFormUsers();     
  }

  //#region Load data
  async cargarUsers() {
    let search = "";
    search = this.filterModel;
    if(search == "") {
      await this._userService.getUser().subscribe(users =>  { this.rowData = users; });
    } else {      
      await this._userService.getUserByName(search).subscribe(users =>  { this.rowData = users; });
    }
  }
  //#endregion

  //#region Load FormGroup
  // Cargar New Profile
  cargarFormUsers() {
    this.formUser = new FormGroup({
      id: new FormControl(null),
      name: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      processed: new FormControl(0)
    });
  }

  // Cargar Update Users
  cargarFormUserUpdate(dataRow) {
    this.formUserUpdate = new FormGroup({
      id: new FormControl(dataRow.id),
      name: new FormControl(dataRow.name, [Validators.required]),
      lastName: new FormControl(dataRow.lastName, [Validators.required]),
      processed: new FormControl(dataRow.processed)
    });
  }
  //#endregion

  //#region toolbar
  clickAdd(){
    this.formUser.patchValue({
      name: '',
      lastName: ''      
    });
    this.open(this.ModalCreate, 'lg');
  }

  clickReport(){
    this.cargarUsers();
  }

  clickChangePro(){
    this.rowDataUpdateBatch = [];
    if(this.gridApi.getSelectedNodes().length > 0) {
      this.gridApi.getSelectedNodes().forEach(element => {
        element.data.processed = true;
        let dataUpdate = {
          index: element.rowIndex,
          data: element.data
        }  
        this.rowDataUpdateBatch.push(dataUpdate);        
      });
      Swal.fire({
        title: '¿Esta seguro de realizar esta accion?',
        text: '¡Este proceso es irreversible!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, procesar!'
      }).then((result) => {
        this.updateUserAll(this.rowDataUpdateBatch);
      });
    } else {
      this.toastr.error('No ha seleccionado ningun usuario', 'Error');
    }     
  }

  clickChangeDesPro(){
    this.rowDataUpdateBatch = [];
    if(this.gridApi.getSelectedNodes().length > 0) {
      this.gridApi.getSelectedNodes().forEach(element => {
        element.data.processed = false;
        let dataUpdate = {
          index: element.rowIndex,
          data: element.data
        }  
        this.rowDataUpdateBatch.push(dataUpdate);        
      });
      Swal.fire({
        title: '¿Esta seguro de realizar esta accion?',
        text: '¡Este proceso es irreversible!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, procesar!'
      }).then((result) => {
        this.updateUserAll(this.rowDataUpdateBatch);
      });
    } else {
      this.toastr.error('No ha seleccionado ningun usuario', 'Error');
    }     
  }

  clickClear(){
    this.filterModel = "";
   }
  //#endregion

  //#region Actions
  onCellClicked(data) {
    const colName = data.colDef.headerName;
    const dataRow = data.data;
    const rowIndex = data.rowIndex;

    switch (colName) {
      case 'OPT':
          if (data.event.target.className.includes('delete') || data.event.target.parentNode.className.includes('delete')) {
              this.deleteRow(rowIndex, dataRow);
          } else if (data.event.target.className.includes('edit') || data.event.target.parentNode.className.includes('edit')) {
              this.indexUpdate = null;
              this.dataModal = dataRow;
              this.indexUpdate = rowIndex;
              this.cargarFormUserUpdate(this.dataModal);
              this.open(this.ModalUpdate, 'lg');
          }
          break;
      default:
          break;
    }
  }

  deleteRow(rowid, dataRow) {
    Swal.fire({
      title: '¿Esta seguro de realizar esta accion?',
      text: '¡Este proceso es irreversible!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Si, borrar!'
    }).then((result) => {
      this.deleteUser(dataRow, rowid);
    });
  }

  onSubmitUser() {  
    this.saveUser(this.formUser.value);
    this.formUser.patchValue({
      name: '',
      lastName: ''
    });
    this.modalService.dismissAll();
  }

  onSubmitUserUpdate() {
    this.updateUser(this.formUserUpdate.value);
    this.formUserUpdate.patchValue({
      name: '',
      lastName: '',
      processed: 0
    });
    this.modalService.dismissAll();
  }
  //#endregion

  //#region service
  saveUser(uss: any) {
    this.load = true;
    this._userService.postUser(uss).subscribe(res => {
      if (res.status === 201 || res.status === 200) {
        this.toastr.success(res.body.message, 'Status: ' + res.status);
        if (res.body != null || res.body !== '') {
            this.refreshComponent(res.body, 'save', '');
        }
      } else if (res.status === 404 || res.status === 500 || res.status === 404) {
        this.toastr.error(res.body.message, 'Status: ' + res.status);
      } else if (res.status === 401) {
        this.toastr.info(res.body.message, 'Status: ' + res.status);
      }
      this.load = false;
    }, err => {
      this.toastr.error('Error en el servicio FALLO CONEXION', 'Status: ' + 500);
    });
  }

  deleteUser(uss: any, rowIndex) {
    this.load = true;
    this._userService.deleteUser(uss).subscribe(res => {
      if (res.status === 201 || res.status === 200) {
        this.toastr.success(res.body.message, 'Status: ' + res.status);
        this.refreshComponent(uss, 'delete', rowIndex);
      } else if (res.status === 404 || res.status === 500 || res.status === 404) {
        this.toastr.error(res.body.message, 'Status: ' + res.status);
      } else if (res.status === 401) {
        this.toastr.info(res.body.message, 'Status: ' + res.status);
      }
      this.load = false;
    }, err => {
      this.toastr.error('Error en el servicio FALLO CONEXION', 'Status: ' + 500);
    });
  }

  updateUser(uss: any) {
    this.load = true;
    this._userService.putUser(uss).subscribe(res => {
      if (res.status === 201 || res.status === 200) {
        this.toastr.success(res.body.message, 'Status: ' + res.status);
        if (res.body != null || res.body !== '') {
            this.refreshComponent(res.body, 'update', this.indexUpdate);
        }
      } else if (res.status === 404 || res.status === 500 || res.status === 404) {
        this.toastr.error(res.body.message, 'Status: ' + res.status);
      } else if (res.status === 401) {
        this.toastr.info(res.body.message, 'Status: ' + res.status);
      }
      this.load = false;
    }, err => {
      this.toastr.error('Error en el servicio FALLO CONEXION', 'Status: ' + 500);
    });
  }

  updateUserAll(uss: any) {
    this.load = true;
    var dataUpdate = [];
    uss.forEach(element => {
      dataUpdate.push(element.data);
    });
    this._userService.putUserAll(dataUpdate).subscribe(res => {
      if (res.status === 201 || res.status === 200) {
        this.toastr.success(res.body.message, 'Status: ' + res.status);
        if (res.body != null || res.body !== '') {            
            this.refreshComponent(res.body, 'updateAll', uss);
        }
      } else if (res.status === 404 || res.status === 500 || res.status === 404) {
        this.toastr.error(res.body.message, 'Status: ' + res.status);
      } else if (res.status === 401) {
        this.toastr.info(res.body.message, 'Status: ' + res.status);
      }
      this.load = false;
    }, err => {
      this.toastr.error('Error en el servicio FALLO CONEXION', 'Status: ' + 500);
    });
  }
  //#endregion

  //#region refresh component
  refreshComponent(data, action, rowIndex) {
    switch (action) {
      case 'save':
        this.rowData.push(data);
        this.gridApi.setRowData(this.rowData);
        break;
      case 'delete':
        this.rowData.splice(rowIndex, 1);
        this.gridApi.setRowData(this.rowData);
        break;
      case 'update':
        this.rowData.splice(rowIndex, 1, data);
        this.gridApi.setRowData(this.rowData);
        break;
      case 'updateAll':
        rowIndex.forEach(element => {
          var p = data.map(e => e.id).indexOf(element.data.id);
          if (p != -1) { 
            this.rowData.splice(element.index, 1, data[p]);
          }          
        });
        this.gridApi.setRowData(this.rowData);
        break;        
      default:
          break;
    }
  }
  //#endregion

  //#region utilitarios
  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open(content, dialogSize: 'sm' | 'md' | 'lg' | 'class' = 'md') {  
    let modalOptions = {};
    if(dialogSize == 'class') {
      modalOptions = { windowClass: 'porcentModal-class',  ariaLabelledBy: 'modal-basic-title'};
    } else {
      modalOptions = {size: dialogSize, ariaLabelledBy: 'modal-basic-title'};
    }
    this.modalReference = this.modalService.open(content, modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;    
  }

  isFirstColumn(params) {
    var displayedColumns = params.columnApi.getAllDisplayedColumns();
    var thisIsFirstColumn = displayedColumns[0] === params.column;
    return thisIsFirstColumn;
  };
  //#endregion

}
